#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables
export CATKIN_TEST_RESULTS_DIR="/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect-build/test_results"
export ROS_TEST_RESULTS_DIR="/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect-build/test_results"

# modified environment variables
export CMAKE_PREFIX_PATH="/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect-build/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect-build/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect-build/devel/lib:$LD_LIBRARY_PATH"
export PATH="/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect-build/devel/bin:/opt/ros/hydro/bin:/usr/lib/x86_64-linux-gnu/qt4/bin:/usr/bin:/usr/local/bin:/usr/bin:/bin:/usr/games"
export PKG_CONFIG_PATH="/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect-build/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect-build"
export PYTHONPATH="/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect-build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect-build/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect:$ROS_PACKAGE_PATH"