# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect/ros_kinect_facedetect_node.cpp" "/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect-build/CMakeFiles/ros_kinect_facedetect_node.dir/ros_kinect_facedetect_node.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "ROS_PACKAGE_NAME=\"ros_kinect_facedetect\""
  "ROSCONSOLE_BACKEND_LOG4CXX"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect-build/lib/CMakeFiles/ros_kinect_facedetect_lib.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/george/diplwmatikh/cmake_projects/facedetect/ros_kinect_facedetect/src/ros_kinect_facedetect/include"
  "/opt/ros/hydro/include"
  "/opt/ros/hydro/include/opencv"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
