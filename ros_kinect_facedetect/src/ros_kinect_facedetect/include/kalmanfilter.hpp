#ifndef KALMAN_FILTER_H
#define KALMAN_FILTER_H

#define EIGEN_DEFAULT_TO_ROW_MAJOR
#include <eigen3/Eigen/Dense>

using namespace Eigen;

namespace kalmanfilt {
  class KalmanFilter
  {
  public:
    KalmanFilter();
    //Eigen matrices are column major by default
    //float T is the frame rate (number of frames between
    //2 kalman predictions
    //Possible extension to take into account the rotating speed
    //of the camera
    KalmanFilter(float,
                 const Matrix<float, 4, Dynamic>&,
                 float);

    void init(float,
              const Matrix<float, 4, Dynamic>&,
              float);

    //possibly add time varying capabilities
    //(currently by hardcoding a new model)
    void updateModel(const Matrix4f&,
                     const Vector4f&,
                     const Matrix<float, 2, 4>&,
                     const Matrix4f&,
                     const Matrix<float, 4, Dynamic>&,
                     const Matrix4f&,
                     const Matrix2f&);

    void applyModelPrediction(float);

    void applyMeasurementCorrection(Matrix<float, 2, Dynamic>&);

    void getState(Matrix<float, 2, Dynamic>&);

    void getVariance(Matrix4f&);

    ~KalmanFilter();

  private:
    Matrix4f A_;
    Vector4f B_;
    Matrix<float, 2, 4> H_;
    Matrix4f P_;
    Matrix<float, 4, Dynamic> state_; //Each column is a 4d vector (x_pos, y_pos, x_vel, y_vel)
    Matrix4f Q_;
    Matrix2f R_;

    void pdist(const  Matrix<float, 2, Dynamic>&,
               const Matrix<float, 2, Dynamic>&,
               Array<int, Dynamic, Dynamic, RowMajor>&);

    inline int my_floor(double);
    inline int my_round(double);

    bool is_initialized_;
  };
}


#endif
