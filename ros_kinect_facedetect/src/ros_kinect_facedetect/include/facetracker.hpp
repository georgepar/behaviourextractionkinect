#ifndef FACETRACKER_H
#define FACETRACKER_H

#define EIGEN_DEFAULT_TO_ROW_MAJOR
#include <eigen3/Eigen/Dense>
#include <opencv2/core/core.hpp>
#include "facedetector.hpp"
#include "kalmanfilter.hpp"

using namespace Eigen;
namespace facetrack {

class FaceTracker
{
public:
  FaceTracker(int, int);

  std::vector<cv::Rect> track_faces(cv::Mat&,
                                    const cv::String&,
                                    int);

  std::vector<cv::Rect> track_mouths(cv::Mat&,
                                     const cv::String&,
                                     const std::vector<cv::Rect>&,
                                     int);

  ~FaceTracker();
private:
  int frame_skip_;
  facedetect::FaceDetector fd_;
  kalmanfilt::KalmanFilter kf_faces_;
  kalmanfilt::KalmanFilter kf_mouths_;
  std::vector<Vector4f> last_state_;
  int first_N_frames_;
  void init_kalman(const std::vector<cv::Rect>&,
                   float,
                   float,
                   kalmanfilt::KalmanFilter&);
  void create_pos_state(const std::vector<cv::Rect>&,
                        Matrix<float, 2, Dynamic>&);
};
}
#endif // FACETRACKER_H
