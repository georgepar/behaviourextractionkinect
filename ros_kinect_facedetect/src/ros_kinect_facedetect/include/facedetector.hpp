#ifndef FACEDETECT_H
#define FACEDETECT_H

#include <vector>
#include <opencv2/objdetect/objdetect.hpp>

namespace facedetect 
{

  class FaceDetector
  {
  public:
    //normal constructor
    FaceDetector();

    std::vector<cv::Rect> detectFace(cv::Mat&,
                                     const cv::String&);

    std::vector<cv::Rect> detectEyes(cv::Mat&,
                                     const cv::String&);

    std::vector<cv::Rect> detectEyes(cv::Mat&,
                                     const cv::String&,
                                     const std::vector<cv::Rect>&);

    std::vector<cv::Rect> detectMouth(cv::Mat&,
                                      const cv::String&);

    std::vector<cv::Rect> detectMouth(cv::Mat&,
                                      const cv::String&,
                                      const std::vector<cv::Rect>&);

    std::vector<cv::Rect> detectMouth(cv::Mat&,
                                      const cv::String&,
                                      const std::vector<cv::Rect>&,
                                      const std::vector<cv::Rect>&);

    void draw_detected(cv::Mat&,
                       cv::Mat&,
                       const std::vector<cv::Rect>&,
                       const cv::Scalar&);

    void draw_detected(cv::Mat&,
                       cv::Mat&,
                       const std::vector<cv::Rect>&,
                       const std::vector<cv::Rect>&,
                       const std::vector<cv::Rect>&);

    //destructor
    ~FaceDetector();
  private:
    cv::CascadeClassifier face_classifier;
    cv::CascadeClassifier eye_classifier;
    cv::CascadeClassifier mouth_classifier;

    void init_frame(cv::Mat&,
                    cv::Mat&);

    std::pair<cv::Point, int> rectToCirc(const cv::Rect&,
                                         int,
                                         int);

    std::vector<cv::Rect> detectEyes(cv::Mat&,
                                     const cv::String&,
                                     const std::vector<cv::Rect>&,
                                     bool);

    std::vector<cv::Rect> detectMouth(cv::Mat&,
                                      const cv::String&,
                                      const std::vector<cv::Rect>&,
                                      const std::vector<cv::Rect>&,
                                      bool);
  };

}

namespace lipsdetect
{
  void bgr_to_rg(cv::Mat&,
                 cv::Mat&);

  std::vector<unsigned int> img_hist(cv::Mat&);

  unsigned int choose_thres(const std::vector<unsigned int>&);

  void detectLips(cv::Mat&,
                  cv::Mat&);
}

#endif
