#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/RegionOfInterest.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include "facedetector.hpp"
#include <eigen3/Eigen/Dense>

using namespace Eigen;

static const std::string OPENCV_WINDOW = "My Inage Window";
static const cv::String face_filename = "data/haarcascades/haarcascade_frontalface_alt.xml";
static const cv::String mouth_filename = "data/haarcascades/haarcascade_mcs_mouth.xml";

class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;
  facedetect::FaceDetector fd_;
  int frame_count_;

public:
  ImageConverter() : it_(nh_), frame_count_(1)
  {
    image_sub_ = it_.subscribe("/camera/rgb/image_color", 1,
                               &ImageConverter::imageCb, this);
    image_pub_ = it_.advertise("/ros_kinect_facedetect/output_video", 1);
    cv::namedWindow(OPENCV_WINDOW);
  }
  ~ImageConverter() { cv::destroyWindow(OPENCV_WINDOW); }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
	
    cv::Mat frame_out = cv_ptr->image;
    std::vector<cv::Rect> faces;
    std::vector<cv::Rect> mouths;

    faces = fd_.detectFace(cv_ptr->image, face_filename);
    mouths = fd_.detectMouth(cv_ptr->image, mouth_filename, faces);

    fd_.draw_detected(cv_ptr->image, frame_out, faces, cv::Scalar(255, 0, 0));
    fd_.draw_detected(frame_out, frame_out, mouths, cv::Scalar(0, 255, 0));
    imshow(OPENCV_WINDOW, frame_out);

    cv::waitKey(1);

    frame_count_++;
    
    image_pub_.publish(cv_ptr->toImageMsg());
  }
};

int main (int argc, char** argv)
{
  // Initialize ROS
  ros::init(argc, argv, "ros_kinect_facedetect");

  ImageConverter ic;

  // Spin
  ros::spin ();
}
