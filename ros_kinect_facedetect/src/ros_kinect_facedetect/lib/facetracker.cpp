#define EIGEN_DEFAULT_TO_ROW_MAJOR
#include <eigen3/Eigen/Dense>
#include <opencv2/core/core.hpp>
#include "facetracker.hpp"
#include "facedetector.hpp"
#include "kalmanfilter.hpp"

using namespace Eigen;

namespace facetrack
{
  FaceTracker::FaceTracker(int frame_skip, int first_N_frames) :
    frame_skip_(frame_skip),
    first_N_frames_(first_N_frames) {}

  FaceTracker::~FaceTracker() {}

  std::vector<cv::Rect> FaceTracker::track_faces(cv::Mat &frame,
                                                 const cv::String &haar_faces,
                                                 int frame_count)
  {
    std::vector<cv::Rect> faces;
    faces = fd_.detectFace(frame, haar_faces);
    if(frame_count < first_N_frames_ && faces.size() > 0) {
      init_kalman(faces, 1, 2, kf_faces_);
    }

    kf_faces_.applyModelPrediction(0); //assume uniform speed
    Matrix<float, 2, Dynamic> face_pos;

    if(faces.size() > 0) {
      create_pos_state(faces, face_pos);
      kf_faces_.applyMeasurementCorrection(face_pos);
    }

    kf_faces_.getState(face_pos);
    for(int i = 0; i < face_pos.cols(); i++) {
      faces[i].x = face_pos(0, i);
      faces[i].y = face_pos(1, i);
    }
    return faces;
  }

  std::vector<cv::Rect> FaceTracker::track_mouths(cv::Mat &frame,
                                                  const cv::String &haar_mouths,
                                                  const std::vector<cv::Rect> &faces,
                                                  int frame_count)
  {
    std::vector<cv::Rect> mouths;
    mouths = fd_.detectMouth(frame, haar_mouths, faces);
    if(frame_count < first_N_frames_ && mouths.size() > 0) {
      init_kalman(mouths, 1, 2, kf_mouths_);
    }

    kf_mouths_.applyModelPrediction(0); //assume uniform speed
    Matrix<float, 2, Dynamic> mouth_pos;

    if(mouths.size() > 0) {
      create_pos_state(mouths, mouth_pos);
      kf_mouths_.applyMeasurementCorrection(mouth_pos);
    }

    kf_mouths_.getState(mouth_pos);
    for(int i = 0; i < mouth_pos.cols(); i++) {
      mouths[i].x = mouth_pos(0, i);
      mouths[i].y = mouth_pos(1, i);
    }
    return mouths;
  }

  void FaceTracker::init_kalman(const std::vector<cv::Rect> &detections,
                                float T,
                                float noise,
                                kalmanfilt::KalmanFilter &kf)
  {
    MatrixXf new_state(4, detections.size());
    Matrix<float, 2, Dynamic> pos;
    MatrixXf vel = MatrixXf::Zero(2, detections.size());
    create_pos_state(detections, pos);
    new_state << pos,
                 vel;
    kf.init(T, new_state, noise);
  }

  void FaceTracker::create_pos_state(const std::vector<cv::Rect> &detections,
                                     Matrix<float, 2, Dynamic> &pos)
  {
    pos.resize(2, detections.size());
    for(int i = 0; i < detections.size(); i++) {
      pos(0, i) = static_cast<float>(detections[i].x);
      pos(1, i) = static_cast<float>(detections[i].y);
    }
  }

}
