#define EIGEN_DEFAULT_TO_ROW_MAJOR
#include <eigen3/Eigen/Dense>
#include <vector>
#include "kalmanfilter.hpp"
#include "munkres.hpp"

namespace kalmanfilt {
  using namespace Eigen;

  KalmanFilter::KalmanFilter() {
    is_initialized_ = false;
  }

  KalmanFilter::KalmanFilter(float T, const Matrix<float, 4, Dynamic> &state, float noise)
  {
    init(T, state, noise);
  }

  KalmanFilter::~KalmanFilter() {}

  void KalmanFilter::init(float T, const Matrix<float, 4, Dynamic> &state, float noise)
  {
    A_ << 1.0f, 0.0f, T,    0.0f,
          0.0f, 1.0f, 0.0f, T,
          0.0f, 0.0f, 1.0f, 0.0f,
          0.0f, 0.0f, 0.0f, 1.0f;

    float T_2 = T*T;
    B_ << 0.5f*T_2, 0.5f*T_2, T, T;

    H_ << 1.0f, 0.0f, 0.0f, 0.0f,
                        0.0f, 1.0f, 0.0f, 0.0f;

    state_ = state;

    float T_3 = T_2*T;
    float T_4 = T_3*T;
    P_ << 0.25f*T_4, 0.0f,      0.5f*T_3, 0.0f,
          0.0f,      0.25f*T_4, 0.0f,     0.5f*T_3,
          0.5f*T_3,  0.0f,      T_2,      0.0f,
          0.0f,      0.5f*T_3,  0.0f,     T_2;

    Q_ = P_;

    R_ << noise, 0.0f,
          0.0f,  noise;

    is_initialized_ = true;
  }

  void KalmanFilter::updateModel(const Matrix4f &A,
                                 const Vector4f &B,
                                 const Matrix<float, 2, 4> &H,
                                 const Matrix4f &P,
                                 const Matrix<float, 4, Dynamic> &state,
                                 const Matrix4f &Q,
                                 const Matrix2f &R)
  {
    A_ = A;
    B_ = B;
    H_ = H;
    P_ = P;
    state_ = state;
    Q_ = Q;
    R_ = R;
  }

  void KalmanFilter::applyModelPrediction(float u)
  {
    for(int i = 0; i < state_.cols(); i++) {
      state_.col(i) = A_*state_.col(i) + B_*u;
    }
    P_ = A_*P_*(A_.transpose()) + Q_;
  }

  void KalmanFilter::applyMeasurementCorrection(Matrix<float, 2, Dynamic> &z)
  {
    Matrix<float, 4, 2> H_T = H_.transpose();
    Matrix<float, 4, 2> K = P_*H_T*((H_*P_*H_T + R_).inverse());

    Matrix4f I = Matrix<float, 4, 4>::Identity();
    P_ = (I - K*H_)*P_;

    Matrix<float, 2, Dynamic> tracks = state_.topRows(2);
    Array<int, Dynamic, Dynamic, RowMajor> cost_matrix;
    pdist(tracks, z, cost_matrix);
    hungarian_alg::Munkres munkres(cost_matrix);
    std::vector< std::pair<int, int> > matches = munkres.run();
    for(int i = 0; i < matches.size(); i++) {
      int track = matches[i].first;
      int obs = matches[i].second;

      if(track != -1 && cost_matrix(track, obs) <= 60) {
        state_.col(track) = state_.col(track) + K*(z.col(obs) - H_*state_.col(track));
      }
      else{
        MatrixXf newMat(4, state_.cols() + 1);
        newMat.leftCols(state_.cols()) = state_;
        Vector4f tempVec;
        tempVec << z.col(obs).transpose(), 0.0f, 0.0f;
        newMat.rightCols<1>() = tempVec;
        state_.swap(newMat);
      }
    }

  }

  void KalmanFilter::getState(Matrix<float, 2, Dynamic>& state_out) {
    state_out = state_.topRows<2>();
  }

  void KalmanFilter::getVariance(Matrix4f& P_out) {
    P_out = P_;
  }

  void KalmanFilter::pdist(const Matrix<float, 2, Dynamic> &p0,
                           const Matrix<float, 2, Dynamic> &p1,
                           Array<int, Dynamic, Dynamic, RowMajor> &cost) {
    cost.resize(p0.cols(), p1.cols());
    for(int i = 0; i < p0.cols(); i++) {
      for(int j = 0; j < p1.cols(); j++) {
        cost(i, j) = my_round(std::abs(p0(0, i) - p1(0, j))  +
                              std::abs(p0(1, i) - p1(1, j)));

      }
    }
  }

  inline int KalmanFilter::my_floor(double x) {
    int x_i = static_cast<int>(x);
    return (x_i > x) ? x_i - 1 : x;
  }

  inline int KalmanFilter::my_round(double x) {
    return my_floor(x + 0.5);
  }
}
