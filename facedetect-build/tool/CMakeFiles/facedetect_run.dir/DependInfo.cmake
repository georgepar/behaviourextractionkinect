# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/george/diplwmatikh/cmake_projects/facedetect/facedetect/tool/main.cpp" "/home/george/diplwmatikh/cmake_projects/facedetect/facedetect-build/tool/CMakeFiles/facedetect_run.dir/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "Clang")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/george/diplwmatikh/cmake_projects/facedetect/facedetect-build/lib/CMakeFiles/facedetect_lib.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/george/diplwmatikh/cmake_projects/facedetect/facedetect/include"
  "/opt/ros/hydro/include/opencv"
  "/opt/ros/hydro/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
