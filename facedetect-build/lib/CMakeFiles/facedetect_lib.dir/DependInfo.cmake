# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/george/diplwmatikh/cmake_projects/facedetect/facedetect/lib/facedetect.cpp" "/home/george/diplwmatikh/cmake_projects/facedetect/facedetect-build/lib/CMakeFiles/facedetect_lib.dir/facedetect.cpp.o"
  "/home/george/diplwmatikh/cmake_projects/facedetect/facedetect/lib/kalman_filter.cpp" "/home/george/diplwmatikh/cmake_projects/facedetect/facedetect-build/lib/CMakeFiles/facedetect_lib.dir/kalman_filter.cpp.o"
  "/home/george/diplwmatikh/cmake_projects/facedetect/facedetect/lib/munkres.cpp" "/home/george/diplwmatikh/cmake_projects/facedetect/facedetect-build/lib/CMakeFiles/facedetect_lib.dir/munkres.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "Clang")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/george/diplwmatikh/cmake_projects/facedetect/facedetect/include"
  "/opt/ros/hydro/include/opencv"
  "/opt/ros/hydro/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
