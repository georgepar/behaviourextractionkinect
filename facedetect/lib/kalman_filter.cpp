#include <iostream>
#include <eigen3/Eigen/Dense>
#include "../include/kalman_filter.hpp"


namespace kalman_filt {
using namespace Eigen;
    KalmanFilter::KalmanFilter() {}
    KalmanFilter::KalmanFilter(float T, const Vector4f& state) 
	{
		KalmanFilter::A_ << 1.0f, 0.0f, T,    0.0f,
							0.0f, 1.0f, 0.0f, T,
							0.0f, 0.0f, 1.0f, 0.0f,
							0.0f, 0.0f, 0.0f, 1.0f;

		float T_2 = T*T;
		KalmanFilter::B_ << 0.5f*T_2, 0.5f*T_2, T, T;

		KalmanFilter::H_ << 1.0f, 0.0f, 0.0f, 0.0f,
							0.0f, 1.0f, 0.0f, 0.0f;
		
		KalmanFilter::state_ = state;

		float T_3 = T_2*T;
		float T_4 = T_3*T;
		KalmanFilter::P_ << 0.25f*T_4, 0.0f,      0.5f*T_3, 0.0f,
							0.0f,      0.25f*T_4, 0.0f,     0.5f*T_3,
							0.5f*T_3,  0.0f,      T_2,      0.0f,
							0.0f,      0.5f*T_3,  0.0f,     T_2;
	}

    void KalmanFilter::updateModel(const Matrix4f &A, const Vector4f &B,
              const Matrix<float, 2, 4> &H, const Matrix4f &P, const Vector4f &state)
    {
        KalmanFilter::A_ = A;
        KalmanFilter::B_ = B;
        KalmanFilter::H_ = H;
        KalmanFilter::P_ = P;
        KalmanFilter::state_ = state;
    }

    void KalmanFilter::applyModelPrediction(float u, const Matrix4f &Q)
    {
        KalmanFilter::state_ = KalmanFilter::A_*KalmanFilter::state_ + KalmanFilter::B_*u;
		Matrix4f A_T = KalmanFilter::A_.transpose();
        KalmanFilter::P_ = KalmanFilter::A_*KalmanFilter::P_*A_T + Q;
    }

   void KalmanFilter::applyMeasurementPrediction(Vector2f &z, const Matrix2f &R)
   {
	   Matrix<float, 4, 2> H_T = KalmanFilter::H_.transpose();
       //Matrix<float, 2, 4> HP = KalmanFilter::H_*KalmanFilter::P_;
       //Matrix2f HPH_TR_inv  = (HP*H_T + R).inverse();
       Matrix<float, 4, 2> K = KalmanFilter::P_*H_T*((KalmanFilter::H_*KalmanFilter::P_*H_T + R).inverse());
      //Vector2f model_out = KalmanFilter::H_*KalmanFilter::state_; 
	   KalmanFilter::state_ = KalmanFilter::state_ + K*(z - KalmanFilter::H_*KalmanFilter::state_);
       Matrix4f I = Matrix<float, 4, 4>::Identity();
       KalmanFilter::P_ = (I - K*KalmanFilter::H_)*KalmanFilter::P_;
   }

   Vector4f KalmanFilter::getState() {
       return KalmanFilter::state_;
   }

   Matrix4f KalmanFilter::getVariance() {
       return KalmanFilter::P_;
   }

}
