#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <vector>
#include "../include/facedetect.hpp"

cv::RNG rng(12345); 

namespace facedetect 
{

  FaceDetector::FaceDetector(const std::string& window_name) : window(window_name), window_num(0) {}

  //destructor
  FaceDetector::~FaceDetector()
  {
    cv::destroyAllWindows();
  }

  std::vector<cv::Rect> FaceDetector::detectFace(cv::Mat& frame, const cv::String& face_trained)
  {
    if(!face_classifier.load(face_trained)) throw std::runtime_error("Error loading face classifier\n");

    std::vector<cv::Rect> faces;
    cv::Mat frame_gray;
    init_frame(frame, frame_gray);
    face_classifier.detectMultiScale(frame_gray,
                                     faces,
                                     1.1,
                                     3,
                                     0|CV_HAAR_SCALE_IMAGE,
                                     cv::Size(60, 60));
    return faces;
  }

  std::vector<cv::Rect> FaceDetector::detectEyes(cv::Mat& frame, const cv::String& eye_trained, const std::vector<cv::Rect>& faces)
  {
    return detectEyes(frame, eye_trained, faces, true);
  }

  std::vector<cv::Rect> FaceDetector::detectEyes(cv::Mat& frame, const cv::String& eye_trained)
  {
    return detectEyes(frame, eye_trained, {cv::Rect(cv::Point(0, 0), frame.size())},
                      false);
  }

  std::vector<cv::Rect> FaceDetector::detectEyes(cv::Mat& frame, const cv::String& eye_trained,
                                                 const std::vector<cv::Rect>& faces, bool face_detected)
  {
    if(!eye_classifier.load(eye_trained))
      throw std::runtime_error("Error loading eye classifier\n");

    cv::Mat frame_gray;
    init_frame(frame, frame_gray);
    std::vector<cv::Rect> eyes;
    for(auto const &f : faces) {
      cv::Rect eyes_f = face_detected ?
            cv::Rect(f.x, cvRound(f.y*1.2), f.width, cvRound(f.height*0.6)) :
            cv::Rect(f.x, f.y, f.width, f.height);

      cv::Mat faceROI = frame_gray(eyes_f);
      std::vector<cv::Rect> temp_eyes;
      eye_classifier.detectMultiScale(faceROI,
                                      temp_eyes,
                                      1.1,
                                      3,
                                      0|CV_HAAR_SCALE_IMAGE,
                                      cv::Size(20, 20));

      if(face_detected) {
        for(auto &e : temp_eyes) {
          e.x = e.x + eyes_f.x;
          e.y = e.y + eyes_f.y;
        }
      }
      eyes.insert(eyes.end(), temp_eyes.begin(), temp_eyes.end());
    }
    return eyes;
  }

  std::vector<cv::Rect> FaceDetector::detectMouth(cv::Mat &frame, const cv::String &mouth_trained)
  {
    return detectMouth(frame, mouth_trained, {cv::Rect(cv::Point(0, 0), frame.size())}, {}, false);
  }

  std::vector<cv::Rect> FaceDetector::detectMouth(cv::Mat &frame, const cv::String &mouth_trained,
                                                  const std::vector<cv::Rect> &faces)
  {
    return detectMouth(frame, mouth_trained, faces, {}, true);
  }

  std::vector<cv::Rect> FaceDetector::detectMouth(cv::Mat &frame, const cv::String &mouth_trained,
                                                  const std::vector<cv::Rect> &faces, const std::vector<cv::Rect> &eyes)
  {
    return detectMouth(frame, mouth_trained, faces, eyes, true);
  }

  std::vector<cv::Rect> FaceDetector::detectMouth(cv::Mat &frame, const cv::String &mouth_trained,
                                                  const std::vector<cv::Rect> &faces, const std::vector<cv::Rect> &eyes, bool face_detected)
  {
    if(!mouth_classifier.load(mouth_trained))
      throw std::runtime_error("Error loading mouth classifier\n");

    std::vector<cv::Rect> mouth;
    cv::Mat frame_gray;
    init_frame(frame, frame_gray);
    int current_eye = 0;
    for(auto const &f : faces) {
      cv::Rect mouth_f = f;
      if(!eyes.empty()){
        for(unsigned e = current_eye; e < eyes.size(); e++) {
          cv::Point p(eyes[e].x, eyes[e].y);
          if(p.inside(f)) {
            int new_y = eyes[e].y + eyes[e].height;
            mouth_f = cv::Rect(f.x, new_y, f.width, f.y + f.height - new_y);
          }
        }
      }

      cv::Mat faceROI = frame_gray(mouth_f);

      std::vector<cv::Rect> temp_mouth;
      mouth_classifier.detectMultiScale(faceROI,
                                        temp_mouth,
                                        1.2,
                                        30,
                                        0 | CV_HAAR_SCALE_IMAGE | CV_HAAR_DO_CANNY_PRUNING,
                                        cv::Size(20, 20));

      if(face_detected) {
        for(auto &m : temp_mouth) {
          m.x = m.x + mouth_f.x;
          m.y = m.y + mouth_f.y;
        }
      }
      mouth.insert(mouth.end(), temp_mouth.begin(), temp_mouth.end());
    }
    return mouth;
  }

  void FaceDetector::draw_detected(const cv::Mat& frame_in, const std::vector<cv::Rect>& faces,
                                   const std::vector<cv::Rect>& eyes, const std::vector<cv::Rect>& mouth)
  {
    if(frame_in.empty()) throw std::runtime_error("Empty frame");
    cv::Mat frame_out = frame_in;
    std::string new_window = window + std::to_string(++window_num);
    cv::namedWindow(new_window);
    if(!faces.empty()) {
      for(auto const &f : faces) {
        cv::rectangle(frame_out, f, cv::Scalar(255, 0, 0), 4, 8, 0);
      }
    }

    if(!eyes.empty()) {
      for(auto const &e : eyes) {
        std::pair<cv::Point,int> circ = rectToCirc(e, 0, 0);
        cv::circle(frame_out, circ.first, circ.second, cv::Scalar(0, 0, 255),
                   4, 8, 0);
      }
    }

    if(!mouth.empty()) {
      for(auto const &m : mouth) {
        cv::rectangle(frame_out, m, cv::Scalar(0, 255, 0), 4, 8 , 0);
      }
    }

    imshow(new_window, frame_out);
  }

  void FaceDetector::init_frame(cv::Mat &frame_in, cv::Mat &frame_out)
  {
    cv::cvtColor(frame_in, frame_out, CV_BGR2GRAY);
    cv::equalizeHist(frame_out, frame_out);
  }

  std::pair<cv::Point, int> FaceDetector::rectToCirc(const cv::Rect& rect, int x_offset, int y_offset)
  {
    return std::make_pair(cv::Point(x_offset + rect.x + rect.width*0.5,
                                    y_offset + rect.y + rect.height*0.5),
                          cvRound((rect.width + rect.height)*0.25));
  }
};

/*
 * based on the model described in
 * http://icts.if.its.ac.id/openaccess/2010/files/VI-10%20hal%2063-68.pdf
 * the threshold is calculated with the method described in
 * http://web-ext.u-aizu.ac.jp/course/bmclass/documents/otsu1979.pdf
 */
namespace lipsdetect 
{
  void bgr_to_rg(cv::Mat &bgr, cv::Mat &rg)
  {
    if(bgr.channels() != 3)
      throw std::runtime_error("Expecting a 3 channel BGR image");

    unsigned int nRows = bgr.rows;
    unsigned int nCols = bgr.cols;
    unsigned int step = bgr.channels();

    cv::Mat rg_img(nRows, nCols, cv::DataType<uchar>::type);
    nCols = nCols*step;

    uchar* row_ptr_bgr;
    uchar* row_ptr_rg;
    for(unsigned int y = 0; y < nRows; y++) {
      row_ptr_bgr = bgr.ptr<uchar>(y);
      row_ptr_rg = rg_img.ptr<uchar>(y);
      for (unsigned int x = 0; x < nCols; x+=step) {
        unsigned int R = static_cast<unsigned int>(row_ptr_bgr[x+2]);
        unsigned int G = static_cast<unsigned int>(row_ptr_bgr[x+1]);
        unsigned int B = static_cast<unsigned int>(row_ptr_bgr[x]);
        double r = static_cast<double>(R)/(R+G+B);
        double g = static_cast<double>(G)/(R+G+B);
        double val = (100 + 100*r - 100*g) / 200;
        unsigned int rg_val = 255*val;
        row_ptr_rg[x/step] = static_cast<uchar>(rg_val);
      }
    }
    rg = rg_img;
  }

  std::vector<unsigned int> img_hist(cv::Mat& gray) {
    std::vector<unsigned int> hist(256, 0);

    uchar* row_ptr;
    for (int y = 0; y < gray.rows; y++) {
      row_ptr = gray.ptr<uchar>(y);
      for (int x = 0; x < gray.cols; x++) {
        hist[static_cast<unsigned int>(row_ptr[x])]++;
      }
    }
    return hist;
  }


  unsigned int choose_thres(const std::vector<unsigned int>& hist) {
    double u_t = 0.0;
    unsigned int N = 0;
    for(unsigned int i = 0; i < hist.size(); i++) {
      u_t += hist[i]*i;
      N += hist[i];
    }
    u_t = u_t / N;

    double max_val = 0.0;
    unsigned int max_pos = 0;
    double w_k = 0.0;
    double u_k = 0.0;

    for(unsigned int k = 0; k < hist.size(); k++) {
      double h_val = static_cast<double>(hist[k]);
      w_k += h_val / N;
      u_k += k * h_val / N;
      double temp = u_t * w_k - u_k;
      double sb_k = temp*temp/(w_k*(1.0 - w_k));
      bool found_max = max_val < sb_k;
      max_val = found_max ? sb_k : max_val;
      max_pos = found_max ? k : max_pos;
    }

    return max_pos;
  }

  void detectLips(cv::Mat &roi, cv::Mat &img_out)
  {
    lipsdetect::bgr_to_rg(roi, img_out);
    std::vector<unsigned int> roi_hist = lipsdetect::img_hist(img_out);
    unsigned int thres = lipsdetect::choose_thres(roi_hist);

    cv::threshold(img_out, img_out, thres, 255, cv::THRESH_BINARY);
  }
};
