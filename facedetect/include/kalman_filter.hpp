#ifndef KALMAN_FILTER_H
#define KALMAN_FILTER_H

#include <eigen3/Eigen/Dense>

using namespace Eigen;
namespace kalman_filt {
class KalmanFilter
{
public:
    KalmanFilter();
    //Eigen matrices are column major by default
	//float T is the frame rate (time interval between two subsequent 
	//frames to be processed)
	//Possible extension to take into account the rotating speed
	//of the camera
    KalmanFilter(float, const Vector4f&);

    //possibly add time varying capabilities
	//(currently by hardcoding a new model)
    void updateModel(const Matrix4f&, const Vector4f&,
              const Matrix<float, 2, 4>&, const Matrix4f&, const Vector4f&);

    void applyModelPrediction(float, const Matrix4f&);
    void applyMeasurementPrediction(Vector2f&, const Matrix2f&);
    Vector4f getState();
    Matrix4f getVariance();
private:
    Matrix4f A_;
    Vector4f B_;
    Matrix<float, 2, 4> H_;
    Matrix4f P_;
    Vector4f state_; //4d vector (x_pos, y_pos, x_vel, y_vel)
};
}


#endif
