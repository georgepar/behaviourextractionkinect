#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <vector>
#include "../include/facedetect.hpp"
#include "../include/munkres.hpp"
#include <eigen3/Eigen/Dense>
using namespace Eigen;
using namespace std;
using namespace facedetect;

int main(int argc, char** argv) {
  Array<int, Dynamic, Dynamic, RowMajor> cost(3, 3);
  cost << 400, 150, 400,
          400, 450, 600,
          300, 225, 300;
  hungarian_alg::Munkres munk(cost);
  std::vector< std::pair<int, int> > match = munk.run();
  int sum = 0;
  for(auto &p : match) {
    if(p.first < cost.rows() && p.second < cost.cols()) {
        sum += cost(p.first, p.second);
        cout << "match {" << p.first << ", " << p.second << "}" << endl;
    }
  }
  cout << sum << endl;
  /*
  if(argc == 1) {
	cout << "Usage: facedetect_run <image1> <image2> ... <imageN>" << endl;
	cout << "Images should be in data/images/ directory" << endl;
	return 1;	
  }
  std::string image_dir("data/images/");
  cv::String face_filename = "data/haarcascades/haarcascade_frontalface_alt.xml";
  cv::String eye_filename = "data/haarcascades/haarcascade_mcs_righteye.xml";
  cv::String mouth_filename = "data/haarcascades/haarcascade_mcs_mouth.xml";
  
  cout << "Here 1" << endl;
  
  std::vector<cv::Mat> images;
  images.resize(argc - 1);
  FaceDetector fdetect("Image ");
  std::map< std::string, std::vector<cv::Rect> > faces;
  std::map< std::string, std::vector<cv::Rect> > eyes;
  std::map< std::string, std::vector<cv::Rect> > mouths;
  
  cout << "Here 2" << endl;

  for(int i=0; i<argc-1; i++) {
	std::string image_name(argv[i+1]);
	images[i] = cv::imread(image_dir + image_name);
	if(images[i].empty()) {
	  cout << "Image " << image_name << " not found in " << image_dir << endl;
	  continue;
	}

	cout << "Here 3" << endl;

    cv::resize(images[i], images[i], cv::Size(800, 600));
	cv::GaussianBlur(images[i], images[i], cv::Size(3,3), 0, 0, cv::BORDER_DEFAULT);

	cout << "Here 4" << endl;

	faces[image_name] = fdetect.detectFace(images[i], face_filename);

	cout << "Here face" << endl;

    eyes[image_name] = fdetect.detectEyes(images[i], eye_filename, faces[image_name]);
 
	cout << "Here eyes" << endl;

 	mouths[image_name] = fdetect.detectMouth(images[i], mouth_filename, faces[image_name], eyes[image_name]);

	cout << "Here mouth" << endl;

    fdetect.draw_detected(images[i], faces[image_name], eyes[image_name], mouths[image_name]);
  
	cout << "Here draw" << endl;
  }

  cv::Mat img = images[0];
  cv::Rect mouth_roi(mouths[argv[1]][0].x +4, mouths[argv[1]][0].y + 4, mouths[argv[1]][0].width - 8, mouths[argv[1]][0].height - 8);
  cout << mouth_roi.height << endl;
  cout << "Here roi" << endl;

  cv::Mat mouth1 = mouth_roi.height > 0 ? img(mouth_roi) : images[0];

  cout << "Here crop roi" << endl;
  cv::Mat lips_img;
  lipsdetect::detectLips(mouth1, lips_img);
  cv::imshow("Lips", lips_img);

  cout << "Here lips" << endl;

  lipsdetect::bgr_to_rg(mouth1, mouth1);
  cv::imshow("New1", mouth1);
*/
  //cout << "Here rg" << endl;
  /*
  cv::namedWindow("New", CV_WINDOW_AUTOSIZE);
  cvtColor(mouth1, mouth1, CV_RGB2GRAY);
  std::vector<unsigned int> hist = lipsdetect::img_hist(mouth1);
  cv::medianBlur(mouth1, mouth1, 7);
  cv::GaussianBlur(mouth1, mouth1, cv::Size(15,15), 0, 0, cv::BORDER_DEFAULT);
  cv::equalizeHist(mouth1,mouth1);
  cv::Laplacian(mouth1, mouth1, CV_8U, 5, 1, 0, cv::BORDER_DEFAULT);
  
  //cv::Mat open_elem(3, 3, CV_8U, cv::Scalar(1));
 // cv::Mat close_elem(7, 7, CV_8U, cv::Scalar(1));
  cv::Mat open_elem = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5), cv::Point(-1, -1));
  cv::Mat close_elem = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(7, 7), cv::Point(-1, -1));
 // cv::erode(mouth1, mouth1, open_elem);
  cv::morphologyEx(mouth1, mouth1, cv::MORPH_CLOSE, close_elem);
  cv::morphologyEx(mouth1, mouth1, cv::MORPH_OPEN, open_elem);
  
  cv::imshow("New", mouth1);
 */
 // cv::waitKey(2000000);
  return 0;
}

